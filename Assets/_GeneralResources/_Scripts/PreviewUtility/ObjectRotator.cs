﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour
{
    // parameters
    public Vector3 axis;
    public float speed;
    public bool randomAxis;

    // cache
    private Transform tran;



    private void Start()
    {
        tran = transform;

        if (randomAxis)
            ChooseRandomAxis();
    }

    private void Update()
    {
        tran.rotation *= Quaternion.Euler(axis * speed * Time.deltaTime);
    }

    private void ChooseRandomAxis()
    {
        float x, y, z;
        x = Random.Range(0, 1f);
        y = Random.Range(0, 1f);
        z = Random.Range(0, 1f);

        axis = new Vector3(x, y, z);
    }
}
