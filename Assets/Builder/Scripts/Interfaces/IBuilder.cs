﻿namespace BuilderPattern
{
    /// <summary>
    /// Contains methods to build a certain Level object
    /// </summary>
    public interface IBuilder
    {
        void InstantiateLevel();
        void ProcessLevel();
        Level GetResult();
    }
}