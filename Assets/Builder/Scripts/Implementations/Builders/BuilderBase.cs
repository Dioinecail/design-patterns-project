﻿namespace BuilderPattern
{
    using UnityEngine;

    public abstract class BuilderBase : IBuilder
    {
        protected Level createdLevel;



        public void InstantiateLevel()
        {
            GameObject levelGO = new GameObject("Level");
            createdLevel = levelGO.AddComponent<Level>();
        }

        public Level GetResult()
        {
            return createdLevel;
        }

        public abstract void ProcessLevel();
    }
}