﻿namespace BuilderPattern
{
    /// <summary>
    /// Uses IBuilder interface to build different Level objects based on input data
    /// </summary>
    public class Director
    {
        private IBuilder builder;

        public Director(IBuilder builder)
        {
            this.builder = builder;
        }

        public Level BuildLevel()
        {
            builder.InstantiateLevel();
            builder.ProcessLevel();
            return builder.GetResult();
        }
    }
}