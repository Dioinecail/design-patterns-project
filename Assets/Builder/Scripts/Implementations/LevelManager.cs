﻿namespace BuilderPattern
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Uses IDirector to build different levels 
    /// </summary>
    public class LevelManager : MonoBehaviour
    {
        Director buildDirector;



        public void CreateRedDirector()
        {
            buildDirector = new Director(new RedBuilder());
        }

        public void CreateBlueDirector()
        {
            buildDirector = new Director(new BlueBuilder());
        }

        public void CreateGreenDirector()
        {
            buildDirector = new Director(new GreenBuilder());
        }

        public void BuildLevel()
        {
            Level level = buildDirector.BuildLevel();
        }
    }
}