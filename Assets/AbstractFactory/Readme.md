﻿This is an attempt of implementing AbstractFactory in game development terms.
After some discussion we came to conclusion that different Race setups is a good candidate for using AbstractFactory pattern.

IRaceFactory interface (AbstractFactory) contains all the necessary factory methods that return abstract objects such as units and structures.

For example:
HumanRaceFactory has human units and human structures
RobotRaceFactory has robot units and robot structures

Units derive from IUnit inteface and thus HumanUnit and RobotUnit has the same interface but different implementations.
The same goes for Structure classes.