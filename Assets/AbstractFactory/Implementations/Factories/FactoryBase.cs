﻿namespace AbstractFactory
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class FactoryBase : MonoBehaviour, IUnitFactory
    {
        public void CreateUnit()
        {
            GetUnit();
        }

        public abstract IUnit GetUnit();
    }
}