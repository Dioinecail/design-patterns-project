﻿namespace AbstractFactory
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class SphereFactory : FactoryBase
    {
        public override IUnit GetUnit()
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.name = "SphereUnit";
            go.AddComponent<Rigidbody>();
            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
            go.AddComponent<SphereCollider>();
            go.layer = 10;

            SphereUnit unit = go.AddComponent<SphereUnit>();

            EventTrigger trigger = go.AddComponent<EventTrigger>();

            EventTrigger.Entry newTrigger = new EventTrigger.Entry();
            newTrigger.eventID = EventTriggerType.Drag;
            newTrigger.callback.AddListener((eventData) =>
            {
                unit.InteractWith();
            });

            trigger.triggers.Add(newTrigger);

            return unit;
        }
    }
}