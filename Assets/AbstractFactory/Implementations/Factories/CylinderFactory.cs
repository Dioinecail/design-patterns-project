﻿namespace AbstractFactory
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class CylinderFactory : FactoryBase
    {
        public override IUnit GetUnit()
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            go.name = "CylinderUnit";
            go.AddComponent<Rigidbody>();
            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
            go.AddComponent<CapsuleCollider>();
            go.layer = 10;

            CylinderUnit unit = go.AddComponent<CylinderUnit>();

            EventTrigger trigger = go.AddComponent<EventTrigger>();

            EventTrigger.Entry newTrigger = new EventTrigger.Entry();
            newTrigger.eventID = EventTriggerType.Drag;
            newTrigger.callback.AddListener((eventData) =>
            {
                unit.InteractWith();
            });

            trigger.triggers.Add(newTrigger);

            return unit;
        }
    }
}