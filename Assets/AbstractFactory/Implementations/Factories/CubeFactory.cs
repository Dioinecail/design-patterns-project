﻿namespace AbstractFactory
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class CubeFactory : FactoryBase
    {
        public override IUnit GetUnit()
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            go.name = "CubeUnit";
            go.AddComponent<Rigidbody>();
            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
            go.AddComponent<BoxCollider>();
            go.layer = 10;

            CubeUnit unit = go.AddComponent<CubeUnit>();

            EventTrigger trigger = go.AddComponent<EventTrigger>();

            EventTrigger.Entry newTrigger = new EventTrigger.Entry();
            newTrigger.eventID = EventTriggerType.Drag;
            newTrigger.callback.AddListener((eventData) =>
            {
                unit.InteractWith();
            });

            trigger.triggers.Add(newTrigger);

            return unit;
        }
    }
}