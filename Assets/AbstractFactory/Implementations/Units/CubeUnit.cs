﻿namespace AbstractFactory
{
    using UnityEngine;

    public class CubeUnit : UnitBase
    {
        public Vector3 scalingAxis = new Vector3(0, 1, 0);
        public float scalingSpeed = 5;



        public override void InteractWith()
        {
            ScaleByAxis();
        }

        // Custom implementation
        protected void ScaleByAxis()
        {
            transform.localScale += scalingAxis * scalingSpeed * Time.deltaTime;
        }
    }
}