﻿namespace AbstractFactory
{
    using UnityEngine;

    public abstract class UnitBase : MonoBehaviour, IUnit
    {
        public abstract void InteractWith();
    }
}