﻿namespace AbstractFactory
{
    using UnityEngine;

    public class CylinderUnit : UnitBase
    {
        public Vector3 movementAxis = new Vector3(0, 1, 0);
        public float movementSpeed = 5;



        public override void InteractWith()
        {
            Move();
        }

        // Custom implementation
        protected void Move()
        {
            transform.position += transform.TransformDirection(movementAxis * movementSpeed * Time.deltaTime);
        }
    }
}