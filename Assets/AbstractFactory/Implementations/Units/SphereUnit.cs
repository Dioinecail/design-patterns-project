﻿namespace AbstractFactory
{
    using UnityEngine;

    public class SphereUnit : UnitBase
    {
        public float scaleBump = 0.006f;



        public override void InteractWith()
        {
            Scale();
        }

        // Custom implementation
        protected void Scale()
        {
            transform.localScale = transform.localScale * (1 + scaleBump);
        }
    }
}