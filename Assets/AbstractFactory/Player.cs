﻿namespace AbstractFactory
{
    using UnityEngine;

    public class Player : MonoBehaviour
    {
        public float interactionRange = 3;


        private void Update()
        {
            
        }

        public void InteractWith(IUnit unit)
        {
            unit.InteractWith();
        }

        public void InteractWith(IUnitFactory unitFactory)
        {
            unitFactory.GetUnit();
        }

        #region DEBUG

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, interactionRange);
        }

        #endregion
    }
}