﻿namespace AbstractFactory
{
    public interface IUnitFactory
    {
        IUnit GetUnit();
    }
}