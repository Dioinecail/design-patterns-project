﻿namespace AbstractFactory
{
    public interface IUnit
    {
        void InteractWith();
    }
}